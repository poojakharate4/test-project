<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();

Route::get('/show-data', 'App\Http\Controllers\HomeController@showData');
Route::get('/show-skill', 'App\Http\Controllers\HomeController@showSkill');
Route::post('/save-application', 'App\Http\Controllers\HomeController@saveApplication')->name('save-application');
Route::get('job-assessment','App\Http\Controllers\HomeController@assessment')->name('assessment');
Route::post('/save-assessment', 'App\Http\Controllers\HomeController@saveAssessment')->name('save-assessment');


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
