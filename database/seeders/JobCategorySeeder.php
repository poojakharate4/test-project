<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\JobCategory;
use App\Models\JobCategorySkill;
use App\Models\SkillTest;

class JobCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect([
            [
                'name' => 'Project Assistant',
                'skills' => [
                    [
                        'name' => 'Communication',
                        'tests' => [
                            [
                                'question' =>  'Who Created Laravel?',
                                'answer' => 'Taylor Otwell',
                                'option1' => 'Margon Stan',
                                'option2' => 'Taylor Otwell',
                                'option3' => 'Samsy Twig',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'Command to start Laravel Application?',
                                'answer' => 'php artisan serve',
                                'option1' => 'php artisan new',
                                'option2' => 'php artisan',
                                'option3' => 'php artisan start',
                                'option4' => 'php artisan serve'
                            ],
                            [
                                'question' => 'Command to install laravel project?',
                                'answer' => 'composer create-project laravel/laravel myproject',
                                'option1' => 'composer create-project laravel/laravel myproject',
                                'option2' => 'composer new-project laravel/laravel myproject',
                                'option3' => 'composer create-project new laravel/laravel myproject',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'What is the use of .env file in Laravel?',
                                'answer' => 'Setting Up local Environment variable',
                                'option1' => 'Setting Up local Environment variable',
                                'option2' => 'Optimization',
                                'option3' => 'Hosting',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'By default laravel project runs on which PORT?',
                                'answer' => '8000',
                                'option1' => '5000',
                                'option2' => '8000',
                                'option3' => '7000',
                                'option4' => '3000'
                            ],
                
                        ]
                    ],
                    [
                        'name' => 'Data Analysis',
                        'tests' => [
                            [
                                'question' => 'CLI Command to migrate in Laravel project?',
                                'answer' => 'php artisan migrate',
                                'option1' => 'php artisan create migration',
                                'option2' => 'php artisan serve',
                                'option3' => 'php artisan migrate',
                                'option4' => 'None of the mentioned above'
                            ],
                            [
                                'question' => 'Command to get the route list in Laravel?',
                                'answer' => 'php artisan route:list',
                                'option1' => 'php artisan list',
                                'option2' => 'php artisan route',
                                'option3' => 'php artisan route:list',
                                'option4' => 'php artisan list:all'
                            ],
                            [
                                'question' => 'CLI Command to create controller in laravel?',
                                'answer' => 'php artisan make:controller Mycontroller',
                                'option1' => 'php artisan make:controller Mycontroller',
                                'option2' => 'php artisan create:controller Mycontroller',
                                'option3' => 'php artisan controller Mycontroller',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => ' Command to make migration in laravel?',
                                'answer' => 'php artisan make:migration create_tags_table',
                                'option1' => 'php artisan migration table',
                                'option2' => 'php artisan make-migration digit',
                                'option3' => 'php artisan make:migration create_tags_table',
                                'option4' => 'None of the mentioned above'
                            ],
                            [
                                'question' => 'Command to check the status of migration in laravel application?',
                                'answer' => 'php artisan migrate:status',
                                'option1' => 'php artisan migration status',
                                'option2' => 'php artisan status',
                                'option3' => 'php artisan migrate:status',
                                'option4' => 'None of these'
                            ],
                
                        ]
                    ],
                    [
                        'name' => 'Problem Solving',
                        'tests' => [
                            [
                                'question' => 'Command to roll back migration operation in laravel?',
                                'answer' => 'php artisan migrate:rollback',
                                'option1' => 'php artisan rollback',
                                'option2' => 'php artisan migration:rollback',
                                'option3' => 'php artisan rollback migration',
                                'option4' => 'php artisan migrate:rollback'
                            ],
                            [
                                'question' => 'Command to rollback all migrations operations?',
                                'answer' => 'php artisan migrate:reset',
                                'option1' => 'php artisan migrate:reset',
                                'option2' => 'php artisan migration:reset',
                                'option3' => 'php artisan reset',
                                'option4' => 'php artisan reset:migration'
                            ],
                            [
                                'question' => 'In which file we can set database connections?',
                                'answer' => 'Paying attention and understanding the speaker',
                                'option1' => 'database.php file',
                                'option2' => 'config.php file',
                                'option3' => '.env file',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'Can laravel application support caching?',
                                'answer' => 'Yes',
                                'option1' => 'Yes',
                                'option2' => 'No',
                                'option3' => 'Can\'t Say',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'What is laravel?',
                                'answer' => 'Framework',
                                'option1' => 'Framework',
                                'option2' => 'Programming Language',
                                'option3' => 'Code Editor',
                                'option4' => 'None of these'
                            ],
                
                        ]
                    ],
                    [
                        'name' => 'Project Management',
                        'tests' => [
                            [
                                'question' => 'Command to check the current version of the Laravel Framwork?',
                                'answer' => 'php artisan --version',
                                'option1' => 'php artisan main --version',
                                'option2' => 'php artisan --version status',
                                'option3' => 'php artisan check --version',
                                'option4' => 'php artisan --version'
                            ],
                            [
                                'question' => 'Command to create middleware in laravel application?',
                                'answer' => 'php artisan make:middleware Mymiddleware',
                                'option1' => 'php artisan make:middleware Mymiddleware',
                                'option2' => 'php artisan middleware Mymiddleware',
                                'option3' => 'php artisan create:middleware Mymiddleware',
                                'option4' => 'php artisan middleware:make Mymiddleware'
                            ],
                            [
                                'question' => 'Full form of ORM in laravel?',
                                'answer' => 'Object Relation Mapping',
                                'option1' => 'Object Relation Mapping',
                                'option2' => 'Object Relation Master',
                                'option3' => 'Object Relation Manager',
                                'option4' => 'None of these'
                            ],
                            [
                                'question' => 'Laravel is based on which design pattern?',
                                'answer' => 'MVC(Model View Controller) Pattern',
                                'option1' => 'Singleton Design Pattern',
                                'option2' => 'MVC(Model View Controller) Pattern',
                                'option3' => 'Factory Design pattern',
                                'option4' => 'Observer pattern'
                            ],
                            [
                                'question' => 'Laravel used?',
                                'answer' => 'Blade template',
                                'option1' => 'Blade template',
                                'option2' => 'Raw page',
                                'option3' => 'All of the above',
                                'option4' => 'None of these'
                            ],
                
                        ]
                    ],
                ]
            ],

        ])->each(function ($category) {
            $cat = JobCategory::create([
                'name' => $category['name'],
            ]);
            foreach ($category['skills'] as $skill) {
                $skillCat = JobCategorySkill::create([
                    'job_category_id' => $cat->id,
                    'name' => $skill['name']
                ]);

                    foreach($skill['tests'] as $test)
                    {
                        SkillTest::create([
                            'job_category_skill_id' => $skillCat->id,
                            'question' => $test['question'],
                            'answer' => $test['answer'],
                            'option1' => $test['option1'],
                            'option2' => $test['option2'],
                            'option3' => $test['option3'],
                            'option4' => $test['option4'],
                        ]);
                    }
            }

        });


    }
}
