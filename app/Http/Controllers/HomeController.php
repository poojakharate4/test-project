<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobCategory;
use App\Models\JobCategorySkill;
use App\Models\JobApplication;
use App\Models\SkillTest;

use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = JobCategory::get();

        return view('home',compact('categories'));
    }

    public function showData(Request $request)
    {
        $categories = JobCategory::get();

        if($request->page == 'app-form'){
            return \Response::json(\View::make('application-form', compact('categories'))->render());
        }
    }

    public function showSkill(Request $request)
    {
        $skills = JobCategorySkill::whereJobCategoryId($request->id)->get();

        return response()->json([
            'status' => true,
            'skills' => $skills,
        ]);
    }

    public function saveApplication(Request $request)
    {
        // dd($request->all());
        $formData = request()->except(['_token', 'skills', 'resume']);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'job_category_id' => ['required', 'exists:job_categories,id'],
            'email' => ['required', 'email', 'max:255'],
            'skills' => ['required', 'array'],
            'total_experience' => ['required', 'numeric'],
            'total_relevant_experience' => ['required', 'numeric'],
            'ctc' => ['required', 'numeric'],
            'in_hand_salary' => ['required', 'numeric'],
            'notice_period' => ['required', 'numeric'],
            'resume' => ['required', 'file'],
        ]);

        $formData['skills'] =  implode(',',$request->skills);

        if($request->hasFile('resume')){
            $image = null;
            $file = $request->file('resume');

            $formData['resume'] = $file->store('resumes');
        }

        $application = JobApplication::create($formData);

        Session::put('applicationId', $application->id);

        return redirect('job-assessment')->with('success', 'Application submitted successfully!');
    }

    public function assessment()
    {
        $id = Session::get('applicationId');
        $application = JobApplication::whereId($id)->first();
        if(! $application)
        {
            return redirect('/')->with('error', 'Please submit application before assessment !');
        }
        $skills = explode(',',@$application->skills);
        $skillSet = [];
        foreach($skills as $skillId)
        {
            $skillSet[] = JobCategorySkill::whereId($skillId)->first();
        }
        $n1 = encrypt($application->id);
        
        return view('assessment-form', compact('skillSet', 'n1'));
    } 

    public function saveAssessment(Request $request)
    {
        $id = decrypt($request->n1);
        $application = JobApplication::whereId($id)->first();
        $marks = 0;
        if(isset($request->question)){
            foreach($request->question as $quesId){
                $mcq = SkillTest::whereId($quesId)->first();
               
                if($mcq->answer == $request->answer[$mcq->id][0])
                {
                    $marks += 1;
                }
            }
        }

        $result = $marks/count($request->question) * 100;
        if($result >= 70)
        {
            $application->status = JobApplication::STATUS_PASSED;
            $msg = 'Congratualation you have Passed the Assessment !;';
        }else{
            $application->status = JobApplication::STATUS_FAILED;
            $msg = 'Sorry you have Failed in the Assessment, Better Luck Next Time !;';
        }

        $application->save();

        Session::forget('applicationId');

        Session::put('result', $result);
        Session::put('msg', $msg);

        return redirect('/')->with('success', $msg);
    }
}
