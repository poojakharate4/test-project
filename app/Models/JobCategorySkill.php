<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobCategorySkill extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    public function mcqs()
    {
        return $this->hasMany(SkillTest::Class);
    }
}
