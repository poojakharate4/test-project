<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    use HasFactory;

    protected $guarded = [];

    const STATUS_PENDING = 1;
    const STATUS_PASSED = 2;
    const STATUS_FAILED = 3;

    const STATUSES = [
        self::STATUS_PENDING => 'Pending',
        self::STATUS_PASSED => 'Passed',
        self::STATUS_FAILED => 'Failed',
    ];

}
