@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome To Sky Shine</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="javascript:void(0);" class="btn btn-success" id="joinTeam" onclick="showData('app-form');">Join Our Team</a>
                    @if(Session::get('applicationId'))
                        <a href="{{url('job-assessment')}}" class="btn btn-success">Start your Assessment </a>
                    @endif
                    <div id="appData">
                        @if(count($errors) > 0)
                            @include('application-form')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
@if(Session::get('result'))
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{Session::get('msg')}}</h1>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       <h3> You have Scored {{Session::get('result')}} %</h3> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif
<script>
    

    function showData(page)
    {
        $.ajax({
            type: "get",
            url: "{{ url('/show-data') }}",
            data: {
                "page" : page
            },
            success: function (data) {
                $('#appData').html(data);
                $('#joinTeam').hide();
                $('appForm').parsley();
            }
        });
    }

    function showSkill()
    {
        id = $('#cat').val();
        
        $.ajax({
            type: "get",
            url: "{{ url('/show-skill') }}",
            data: {
                "id": id
            },
            success: function (data) {
                var select = ' <option value="" selected>Select Skill</option>';
                $.each(data.skills, function (key, value) {
                    select += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $('#skill').attr('required','');
                $('#skill').attr('data-parsley-required-message','Please select Skills.');
                $('#skill').html(select);
                $('#skillSet').show();
            }
        });
    }

    if('{{Session::get('result')}}')
    {
        $('#exampleModal').modal('show');
    }

    $('#exampleModal').on('hidden', function () {
        '{{Session::forget('result')}}';
       '{{Session::forget('msg')}}'
    });
    
</script>
@endsection
