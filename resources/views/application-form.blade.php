

                    <form id="appForm" action="{{route('save-application')}}" method="post" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="mb-3">
                            <label for="firstName" class="form-label">First Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="firstName" name="first_name" required  data-parsley-required-message="Please enter first name." value="{{old('first_name')}}">
                            @if($errors->has('first_name'))
                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="lastName" class="form-label">Last Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="lastName" name="last_name" required  data-parsley-required-message="Please enter last name." value="{{old('last_name')}}">
                            @if($errors->has('last_name'))
                                <span class="text-danger">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email"  name="email" required  data-parsley-required-message="Please enter email." value="{{old('email')}}">
                            @if($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="cat" class="form-label">Job Category<span class="text-danger">*</span></label>
                            <select class="form-select" id="cat" aria-label="Default select example" name="job_category_id" onchange="showSkill();"
                            required  data-parsley-required-message="Please select job category.">
                                <option value="" selected>Select Job Category</option>
                                @foreach($categories as $key=> $category)
                                    <option value="{{$category->id}}" @if($category->id == old('job_category_id')) selected @endif>{{$category->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('job_category_id'))
                                <span class="text-danger">{{ $errors->first('job_category_id') }}</span>
                            @endif
                        </div>
                        <div class="mb-3" id="skillSet" style="display:none">
                            <label for="skill" class="form-label">Skills<span class="text-danger">*</span></label>
                            <select class="form-select" name="skills[]" id="skill" multiple aria-label="Multiple select example">
                            </select>
                            @if($errors->has('skills'))
                                <span class="text-danger">{{ $errors->first('skills') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="totalExp" class="form-label">Years of Experience<span class="text-danger">*</span></label>
                            <input type="number" min="0" class="form-control" id="totalExp" name="total_experience" required  data-parsley-required-message="Please enter years of experience." value="{{old('total_experience')}}">
                            @if($errors->has('total_experience'))
                                <span class="text-danger">{{ $errors->first('total_experience') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="totalExpRel" class="form-label">Years of Experience for Selected Job<span class="text-danger">*</span></label>
                            <input type="number" min="0" class="form-control" id="totalExpRel" name="total_relevant_experience" required  data-parsley-required-message="Please enter years of experience for selected job." value="{{old('total_relevant_experience')}}">
                            @if($errors->has('total_relevant_experience'))
                                <span class="text-danger">{{ $errors->first('total_relevant_experience') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="ctc" class="form-label">Current CTC<span class="text-danger">*</span></label>
                            <input type="number" min="0" class="form-control" id="ctc" name="ctc" required  data-parsley-required-message="Please enter Current CTC." value="{{old('ctc')}}">
                            @if($errors->has('ctc'))
                                <span class="text-danger">{{ $errors->first('ctc') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="totalSal" class="form-label">Current in Hand/Fixed Monthly Salary<span class="text-danger">*</span></label>
                            <input type="number" min="0" class="form-control" id="totalSal" name="in_hand_salary" required  data-parsley-required-message="Please enter current in hand/fixed monthly salary." value="{{old('in_hand_salary')}}">
                            @if($errors->has('in_hand_salary'))
                                <span class="text-danger">{{ $errors->first('in_hand_salary') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="noticePeriod" class="form-label">Notice Period<span class="text-danger">*</span></label>
                            <select class="form-select" id="noticePeriod" name="notice_period" aria-label="Default select example"
                            required  data-parsley-required-message="Please select notice period.">
                                <option  value="" selected>Select Notice Period</option>
                                <option value="30" @if(old('in_hand_salary') == 30) selected @endif>30 Days</option>
                                <option value="45" @if(old('in_hand_salary') == 45) selected @endif>45 Days</option>
                                <option value="60" @if(old('in_hand_salary') == 60) selected @endif>60 Days</option>
                                <option value="90" @if(old('in_hand_salary') == 90) selected @endif>90 Days</option>
                            </select>
                            @if($errors->has('notice_period'))
                                <span class="text-danger">{{ $errors->first('notice_period') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="inputGroupFile01">Resume</label>
                            <input type="file" class="form-control" id="inputGroupFile01" name="resume"
                            accept="application/msword, text/plain, application/pdf, image/*"
                            required  data-parsley-required-message="Please upload resume."  data-parsley-errors-container="#error-container-resume">
                            @if($errors->has('resume'))
                                <span class="text-danger">{{ $errors->first('resume') }}</span>
                            @endif
                            <span id="error-container-resume"></span>
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1"  required  data-parsley-required-message="Please select check me out.">
                            <label class="form-check-label" for="exampleCheck1">Check me out<span class="text-danger">*</span></label>
                        </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                
