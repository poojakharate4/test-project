@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Please complete the Assessment</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="appForm" action="{{route('save-assessment')}}" method="post" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        @foreach($skillSet as $skill)
                        <h3>{{$skill->name}}</h3>
                            @foreach($skill->mcqs as $key => $mcq)
                            <div class="mb-3">
                                <label for="question{{$key}}" class="form-label">{{$mcq->question}}<span class="text-danger">*</span></label>
                                <input type="hidden" class="form-control" id="question{{$key}}" name="question[]"  value="{{$mcq->id}}">
                                <input type="hidden" name="n1" value="{{$n1}}">
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="answer[{{$mcq->id}}][]" id="flexRadioDefault1{{$mcq->id}}{{$key}}" value="{{$mcq->option1}}"
                                 required  data-parsley-required-message="Please select one option." data-parsley-errors-container="#error-container-question{{$mcq->id}}">
                                <label class="form-check-label" for="flexRadioDefault1{{$mcq->id}}{{$key}}">
                                   {{$mcq->option1}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="answer[{{$mcq->id}}][]" id="flexRadioDefault2{{$mcq->id}}{{$key}}" value="{{$mcq->option2}}"
                                required  data-parsley-required-message="Please select one option." data-parsley-errors-container="#error-container-question{{$mcq->id}}">
                                <label class="form-check-label" for="flexRadioDefault2{{$mcq->id}}{{$key}}">
                                {{$mcq->option2}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="answer[{{$mcq->id}}][]" id="flexRadioDefault3{{$mcq->id}}{{$key}}"  value="{{$mcq->option3}}"
                                required  data-parsley-required-message="Please select one option." data-parsley-errors-container="#error-container-question{{$mcq->id}}">
                                <label class="form-check-label" for="flexRadioDefault3{{$mcq->id}}{{$key}}">
                                {{$mcq->option3}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="answer[{{$mcq->id}}][]" id="flexRadioDefault4{{$mcq->id}}{{$key}}" value="{{$mcq->option4}}"
                                required  data-parsley-required-message="Please select one option." data-parsley-errors-container="#error-container-question{{$mcq->id}}">
                                <label class="form-check-label" for="flexRadioDefault4{{$mcq->id}}{{$key}}">
                                {{$mcq->option4}}
                                </label>
                            </div>
                            <span id="error-container-question{{$mcq->id}}"></span>

                            <br>

                            @endforeach
                            <hr>
                        @endforeach
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection